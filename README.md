# Deep Bridge

Etapes pour lancer le projet :
1. Installer python 3.* https://www.python.org
2. Installer les bibliotheque en utilisant la commande suivante :
`pip install -r requirements.txt`
3. Créer dans la racine du projet un dossier _patients_ avec les dossier patients à visualiser
4. Supprimer les fichiers .txt et .tmp du dossier patient à visualiser
5. Changer le chemin vers le dossier patient dans le code (ligne 36)


# Contacts :
- **Moussaoui Mohammed** : Moussaoui.mohamed.m@gmail.com
- **Swabahadine Abdallah** : swabmed@gmail.com 
- **Réda Jalal** : Jalal.reda@outlook.com
- **El Haroui Jassim** : jassim.elharoui@gmail.com 
