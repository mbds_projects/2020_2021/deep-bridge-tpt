"""
    Versions :
    Python 3.9.0
    pip 20.2.3
    
    Pour installer les bibliothèque utilisez la commande suivante :
    - pip install -r requirements.txt
    
"""

import pydicom
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import sys
import glob
from scipy import ndimage
import math
import tkinter 
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

np.set_printoptions(threshold=sys.maxsize)

"""
    ROTATION_X : la valeur du centre de la rotation par default
    files : la liste des fichiers d'un dossier patients
    dossier : le chemin vers LE dossier patient à afficher,
            vous devez laisser que les fichier .dcm dans ce dossier.
            (pas de fichier .txt ou .tmp)
"""
ROTATION_X = 332
files = []
dossier = "./patients/dossier_1/*"


"""
    Chargement des fichiers
"""
print('glob: {}'.format(dossier))
for fname in glob.glob(dossier, recursive=False):
    files.append(pydicom.dcmread(fname, force=True))
print("file count: {}".format(len(files)))


"""
    Supprimer les fichier qui n'ont pas une SliceLocation
    SliceLocation nous permet de connaitre l'emplacement du fichier 
    dans l'ensemble du dossier patients.
"""
slices = []
skipcount = 0
for f in files:
    if hasattr(f, 'SliceLocation'):
        slices.append(f)
    else:
        skipcount = skipcount + 1
print("skipped, no SliceLocation: {}".format(skipcount))


"""
    ordonner les fichiers
"""
slices = sorted(slices, key=lambda s: s.SliceLocation)

img_shape = list(slices[0].pixel_array.shape)
img_shape.append(len(slices))


"""
    Cette fonction nous permet de faire une rotation sur un plan 2D
"""
def rotateX(originX, originY, x, y, angleD):
    angle = angleD * math.pi / 180
    if (originY <= y):
        cos = math.cos(angle) * (y - originY)
        sin = math.sin(angle) * (y - originY)
        return cos + originY, sin + originX
    else:
        cos = math.cos(angle + math.pi) * (originY - y)
        sin = math.sin(angle + math.pi) * (originY - y)
        return cos + originY, sin + originX

"""
    Cette fonction nous permet de faire une rotation sur un plan 3D
"""
def img3DWithRotation(angle, depth):
    imgFinal = []
    for i, s in enumerate(slices):
        img2d = s.pixel_array
        rowArray = []
        for j in range(len(img2d)):
            cos, sin = rotateX(depth, ROTATION_X, depth, j, angle)
            if (math.ceil(sin) >= 512 or math.ceil(cos) >=512):
                rowArray.append(0)
            else:
                rowArray.append(img2d[math.ceil(sin)][math.ceil(cos)])
        imgFinal.append(rowArray)
    return imgFinal


"""
    Cette fonction nous permet de convertir un scan unités Hounsfield (HU)
    https://fr.wikipedia.org/wiki/Échelle_de_Hounsfield

"""
def get_pixels_hu(image):
    """
        Convertir en int16 
        devrait être possible car les valeurs doivent toujours être suffisamment faibles (<32k)
    """
    image = image.astype(np.int16)

    """
        Définir les pixels hors scan sur 0
        L'interception est généralement de -1024, donc l'air est d'environ 0
    """
    image[image == -2000] = 0

    for slice_number in range(len(slices)):
        intercept = slices[slice_number].RescaleIntercept
        slope = slices[slice_number].RescaleSlope

        if slope != 1:
            image[slice_number] = slope * image[slice_number].astype(np.float64)
            image[slice_number] = image[slice_number].astype(np.int16)

        image[slice_number] += np.int16(intercept)

    return np.array(image, dtype=np.int16)

""" 
    Récupérer une image 2D selon la profondeur et l'angle
"""
def coronalHuDepth(angle, depth):
    imgFinal = img3DWithRotation(angle, depth)
    coronal = np.array(imgFinal)
    coronal_hu = get_pixels_hu(coronal)
    return coronal_hu[::-1]



"""
    Lancer Tkinter
"""
root = tkinter.Tk()
root.title('Deep Bridge')
lines = []

"""
    Configuration pour le Plot
"""
fig, ax = plt.subplots()
canvas = FigureCanvasTkAgg(fig, master=root)  
canvas.draw()
canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
plt.subplots_adjust(left=0.1, right=0.9, top=0.7, bottom=0.1)



"""
    Initialiser le plot avec une image avec une angle de 73,
    et profondeur profondeur_max/2
"""
coronal_hu = coronalHuDepth(73, img_shape[0]//2)
lines = ax.plot([ROTATION_X,ROTATION_X], [0,570], "c")

ax.imshow(coronal_hu, cmap=plt.cm.gray, vmin=0, vmax=1000)


"""
    Ajouter les slider pour modifier la densité Min, Max l'angle et la profondeur

    Pour des infos sur le Min et Max :
    https://stackoverflow.com/questions/58791377/medical-image-quality-problem-with-dicom-files
"""
axcolor = 'lightgoldenrodyellow'
axCenter = plt.axes([0.25, 0.75, 0.65, 0.03], facecolor=axcolor)
axMin = plt.axes([0.25, 0.8, 0.65, 0.03], facecolor=axcolor)
axMax = plt.axes([0.25, 0.85, 0.65, 0.03], facecolor=axcolor)
axAngle = plt.axes([0.25, 0.9, 0.65, 0.03], facecolor=axcolor)
axDepth = plt.axes([0.25, 0.95, 0.65, 0.03], facecolor=axcolor)


sCenter = Slider(axCenter, 'Center', 0, 512, valinit=ROTATION_X, valstep=1)
sMin = Slider(axMin, 'Min', -1000, 1000, valinit=0, valstep=10)
sMax = Slider(axMax, 'Max', -1000, 1000, valinit=1000, valstep=10)
sAngle = Slider(axAngle, 'Angle', 0, 360, valinit=73, valstep=1)
sDepth = Slider(axDepth, 'Depth', 150, 350, valinit=img_shape[0]//2, valstep=1)


"""
    La fonction qui gére le mise à jour de l'image
"""
def update(val):
    global lines
    global ROTATION_X
    angleV = sAngle.val
    depthV = sDepth.val
    minV = sMin.val
    maxV = sMax.val
    ROTATION_X = sCenter.val
    lines.pop(0).remove()
    ax.imshow(coronalHuDepth(angleV, depthV), cmap=plt.cm.gray, vmin=minV, vmax=maxV)
    lines = ax.plot([ROTATION_X,ROTATION_X], [0,570], "c")


sCenter.on_changed(update)
sAngle.on_changed(update)
sDepth.on_changed(update)
sMin.on_changed(update)
sMax.on_changed(update)




toolbar = NavigationToolbar2Tk(canvas, root)
toolbar.update()
canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)


def on_key_press(event):
    print("you pressed {}".format(event.key))
    key_press_handler(event, canvas, toolbar)


canvas.mpl_connect("key_press_event", on_key_press)

bottomframe = tkinter.Frame(root)
bottomframe.pack( side = tkinter.BOTTOM )


tkinter.mainloop()

